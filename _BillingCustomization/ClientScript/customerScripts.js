/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 Apr 2015     Jose Castro
 *
 */


/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType 
 * 
 * @param {String} type Access mode: create, copy, edit
 * @returns {Void}
 */
function clientPageInit(type) {
	if(type=="edit") {
		var bal = (nlapiGetFieldValue('balance')!=null) ? nlapiGetFieldValue('balance') : 0;
		//custentity18 == Refused COD 
		var refusedCODDate = nlapiGetFieldValue('custentity18');
		//custentity14 == Birthdate
		var custBirthDate = nlapiGetFieldValue('custentity14');
		var alertStringBuf = new StringBuilder();
		
		console.log("HAS Refused CODDate" + hasRefusedCOD(refusedCODDate));
		if(hasRefusedCOD(refusedCODDate)) {
			alertStringBuf.append('\nBe aware this Customer has REFUSED COD');
		} 
		console.log("HAS Open Balance" + hasOpenBalance(bal));
		if(hasOpenBalance(bal)) {
			alertStringBuf.append('\nBe aware this Customer has an OPEN BALANCE');
		}
		
		if(custBirthDate != null && custBirthDate != NaN) {
			var customerAge = getCustomerAge(custBirthDate);
			var isCustomerMinorOfAge = customerAge < 18;
			console.log(customerAge);
			console.log(isCustomerMinorOfAge);
			if(isCustomerMinorOfAge) {
				alertStringBuf.append('\nBe aware this Customer is a minor.');
			}
		}
		
		console.log("######" + alertStringBuf.toString());
		if(alertStringBuf.toString().length != 0) alert(alertStringBuf.toString());
	}
}

function hasOpenARBal(bal, refusedCODDate) {
	return (bal > 0 && refusedCODDate.length !== 0);
}

function hasRefusedCOD(refusedCODDate) {
	return refusedCODDate.length !== 0;
}

function hasOpenBalance(bal) {
	return bal > 0;
}

function getCustomerAge(custBirthDate) {
	var today = new Date();
	console.log(custBirthDate);
	console.log(today);
	var custBDate = new Date(custBirthDate);
	var age = today.getFullYear() - custBDate.getFullYear();
	var ageDiff = today.getMonth() - custBDate.getMonth();
	if(ageDiff < 0 || (ageDiff === 0 && today.getDate() < custBDate.getDate())) {
		age--;
	}
	return age;
}