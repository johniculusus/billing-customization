/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       12 Jun 2015     Jose Castro
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Sublist internal id
 * @returns {Void}
 */
function clientRecalc(type){
	if(type=="custpage_fake_claims") {
		var sublistLineItemCount = nlapiGetLineItemCount('custpage_fake_claims');
		
		var total = 0;
		for(var a=1; a<sublistLineItemCount+1; a++) {
			var currentLineItemVal = nlapiGetLineItemValue('custpage_fake_claims', 'custpage_fakeclaim_payment', a);
			if(currentLineItemVal!=null && currentLineItemVal!="") total+=parseFloat(currentLineItemVal);
		}
		nlapiSetFieldValue('custpage_sublist_totalamt', total, false, false);
	}
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Void}
 */
function clientFieldChanged(type, name, linenum){
	if(type=="custpage_fake_claims") {
		var currentFieldVal = nlapiGetCurrentLineItemValue('custpage_fake_claims', 'custpage_fakeclaim_payment');
		if(parseFloat(currentFieldVal) > 2000) {
			alert("Payment can't be greater than 2000.");
			nlapiSetCurrentLineItemValue('custpage_fake_claims', 'custpage_fakeclaim_payment', "", false, false);
			return false;
		}
	}
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Boolean} True to continue changing field value, false to abort value change
 */
function clientValidateField(type, name, linenum){
	return true;
}
