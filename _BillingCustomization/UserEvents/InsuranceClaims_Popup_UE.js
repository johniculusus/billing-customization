/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 May 2015     Jose Castro
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request) {
	var isTypeView = (type == "view");
	var currentUser = nlapiGetUser();
	var joseCastroUserID = 2;
	var recType = nlapiGetRecordType();
	var suiteletURL = nlapiResolveURL('SUITELET','customscript_suitelet_insuranceclaim','customdeploy1');
	
	//TO REMOVE when alpha testing succeed on Client/Business Side
//	if(currentUser == joseCastroUserID) {
		if(isTypeView) {
			if(recType == 'invoice') {
				var dateTimeZoneUtil = Object.create(DateTimeZoneUtils);
				var companyTimeNow = dateTimeZoneUtil.getCompanyCurrentDate();
				
				var myInlineHtml=form.addField('custpagehtml', 'inlinehtml', '', null, null);
				var jqueryinline = nlapiGetContext().getSetting('SCRIPT', 'custscript_inline_insuranceclaim_popup');
					jqueryinline = jqueryinline.replace('{suiteleturl}', suiteletURL);

					var customerID = nlapiGetFieldValue('entity');
					
					var filters = [
		                new nlobjSearchFilter('formulatext', null, 'is', customerID).setFormula('{internalid}')
		            ];
					var primaryInsurance = ""; // name of Primary Insurance Company
					var primaryInsuranceID = "";
					var secondaryInsurance = ""; // name of Secondary Insurance Company
					var secondaryInsuranceID = "";
					var srch = nlapiSearchRecord('customer', 'customsearch_ss_customer_search_prim_sec', filters, null);
					for (var i = 1; srch!=null && i <= srch.length; i++) {
						var searchresultE = srch[ i-1 ];
						var columnsE = searchresultE.getAllColumns();
						primaryInsurance = searchresultE.getValue(columnsE[2]); // primary insurance company
						secondaryInsurance = searchresultE.getValue(columnsE[3]); // secondary insurance company
						primaryInsuranceID = searchresultE.getValue(columnsE[4]);
						secondaryInsuranceID = searchresultE.getValue(columnsE[5]);
					}
					
					var status = "";
					var ctr=0;
					var filters2 = [
		                new nlobjSearchFilter('formulatext', null, 'is', nlapiGetRecordId()).setFormula('{internalid}')
		            ];
					var srch2 = nlapiSearchRecord('transaction', 'customsearch_ss_custinv_notpaid', filters2, null);
					for (var i = 1; srch2!=null && i <= srch2.length; i++) {
						var searchresultE = srch2[ i-1 ];
						var columnsE = searchresultE.getAllColumns();
						var custInvoiceStatus = searchresultE.getValue(columnsE[9]);
						if(ctr==0) status = custInvoiceStatus;
						else break;
						ctr++;
					}
					
					var hasInsuranceClaimProcessed = false;
					if(nlapiGetFieldValue('custbody37') != "1" && nlapiGetFieldValue('custbody37') != "") {
						hasInsuranceClaimProcessed = true;
					}
					
					jqueryinline = jqueryinline.replace('{primaryInsurance}', (primaryInsurance!=null && primaryInsurance!="") ? primaryInsurance : "");
					jqueryinline = jqueryinline.replace('{primaryInsuranceID}', (primaryInsuranceID!=null && primaryInsuranceID!="") ? primaryInsuranceID : "");
					jqueryinline = jqueryinline.replace('{secondaryInsurance}', (secondaryInsurance!=null && secondaryInsurance!="") ? secondaryInsurance : "");
					jqueryinline = jqueryinline.replace('{secondaryInsuranceID}', (secondaryInsuranceID!=null && secondaryInsuranceID!="") ? secondaryInsuranceID : "");
					var NSTotalAmt = nlapiGetFieldValue('total');
					jqueryinline = jqueryinline.replace('{total}', NSTotalAmt);
					jqueryinline = jqueryinline.replace('{total_validation}', (NSTotalAmt!=null) ? NSTotalAmt : 1);
					jqueryinline = jqueryinline.replace('{customerInvoice}', nlapiGetRecordId());
					jqueryinline = jqueryinline.replace('{createdfrom}', nlapiGetFieldValue('createdfrom'));
					
					if(( (primaryInsuranceID!=null && primaryInsuranceID!="") || (secondaryInsuranceID!=null && secondaryInsuranceID!="") )
						 && (status!="" && "open".equalsIgnoreCase(status.trim())) && !hasInsuranceClaimProcessed
					  ) {
						myInlineHtml.setDefaultValue(jqueryinline);
						var showPopupClaim=form.addButton('custpage_claimbtn', 'INS. CLAIM', "$('#draggable').dialog({position: { my: 'center bottom', at: 'center bottom', of: window }})");
					}
			}
		}
//	}
	//END TO REMOVE when alpha testing succeed on Client/Business Side
}
