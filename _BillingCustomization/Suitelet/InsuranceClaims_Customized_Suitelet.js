/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       09 Jun 2015     Jose Castro
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
	var requestMethod = request.getMethod();
	var formTitle = "Insurance Claims Payments";
	var cust = request.getParameter('cust');
	var custName = request.getParameter('custName');
	
	if(requestMethod == 'GET') {
		var form = nlapiCreateForm(formTitle, false);
			form.setScript('customscript_cs_insuranceclaims');
		var group = form.addFieldGroup('custpage_group', 'Filter Nav', null);
			group.setSingleColumn(false);
			group.setShowBorder(true);
		
		var inline = form.addField('custpage_note', 'inlinehtml', '', null, null);
			inline.setDefaultValue('<span style="color: red; font-size: 18px">Before submitting this form, please make sure to match the check amount to the Amount Received.</span>');
			inline.setLayoutType('outside', 'startrow');
		
		var filteredCustID = form.addField('custpage_filter_custid', 'text', '', null, null);
			filteredCustID.setDisplayType('hidden');
			filteredCustID.setDefaultValue(cust);
			
		var filteredCustName = form.addField('custpage_filter_custname', 'text', '', null, null);
			filteredCustName.setDisplayType('hidden');
			filteredCustName.setDefaultValue(custName);
		
		var insuranceSelect = form.addField('custpage_insurancecustomers', 'select', 'Insurance Company', null, 'custpage_group');
			insuranceSelect.addSelectOption('', '', true);
		
		var referenceNum = form.addField('custpage_referencenum', 'text', 'Reference', null, 'custpage_group');
			referenceNum.setMandatory(true);
		
		var totalAmtOfSublist = form.addField('custpage_sublist_totalamt', 'currency', 'Amount Received', null, 'custpage_group');
			totalAmtOfSublist.setDisplayType('inline');
			totalAmtOfSublist.setDefaultValue(0);
		
		if((cust!=null && custName!=null) && (cust!="" && custName!="")) var submit = form.addSubmitButton('Submit');
		
		var setWindowChanged = "setWindowChanged(window,false);";
		var setHrefStart = "document.location.href='" + nlapiResolveURL('SUITELET','customscript_suitlet_insuranceclaims','customdeploy1');
		var data1 = "&cust='+ nlapiGetFieldValue('custpage_insurancecustomers') +'";
		var data2 = "&custName='+ nlapiGetFieldText('custpage_insurancecustomers') +'";
		var setHrefEnd = "'";
		var script = setWindowChanged + " " + setHrefStart + data1 + data2 + setHrefEnd;

		var filter = form.addButton('custpage_btnfilter', 'Filter', script);

		var sublistClaims = createSublistClaims(form);
		if((cust!=null && custName!=null) && (cust!="" && custName!="")) setSublistClaims(sublistClaims, custName);
		
		var srch = nlapiSearchRecord('customer', 'customsearch_ss_cust_suitelet_insclaims', null, null);
		for (var i = 1; srch!=null && i <= srch.length; i++) {
			var searchresultE = srch[ i-1 ];
			var columnsE = searchresultE.getAllColumns();
			var id = searchresultE.getValue(columnsE[0]);
			var name = searchresultE.getValue(columnsE[1]);
			
			insuranceSelect.addSelectOption(id, name, (id==cust) ? true : false);
		}
		
		response.writePage(form);
	} else if(requestMethod == 'POST') {
		var baset = getBaseT(request);
		
		if(baset.indexOf('deploy')!=-1) {
		// cookie doesn't exist, create it now
			var dateTimeZoneUtil = Object.create(DateTimeZoneUtils);
			var companyTimeNow = dateTimeZoneUtil.getCompanyCurrentDate();
			
			var categoryParam = request.getParameter('custpage_insurancecustomers');
			var insuranceCustomerIDParam = request.getParameter('custpage_filter_custid');
			var insuranceCustomerNameParam = request.getParameter('custpage_filter_custname');
			var refNumParam = request.getParameter('custpage_referencenum');
			var totalamtParam = request.getParameter('custpage_sublist_totalamt');
			
			var formTitle = "Insurance Claims Payments has been Submitted";
			var form = nlapiCreateForm(formTitle, false);
			
			var group = form.addFieldGroup('custpage_group', 'Filter Nav', null);
			group.setSingleColumn(false);
			group.setShowBorder(true);
			
	//		var insuranceCompanyRec = nlapiLoadRecord('customer', insuranceCustomerIDParam, null);
			var checkInsurancecust = form.addField('custpage_check_insurancecustomer', 'text', 'Insurance Company', null, 'custpage_group');
				checkInsurancecust.setDisplayType('inline');
				checkInsurancecust.setDefaultValue(insuranceCustomerNameParam);
			var checkReference = form.addField('custpage_check_reference', 'text', 'Reference', null, 'custpage_group');
				checkReference.setDefaultValue(refNumParam);
				checkReference.setDisplayType('inline');
			var checkTotalAmt = form.addField('custpage_check_totalamt', 'currency', 'Total Amount to be Paid', null, 'custpage_group');
				checkTotalAmt.setDefaultValue(totalamtParam);
				checkTotalAmt.setDisplayType('inline');
			 
			var sublistCheckClaimed = createSublistCheckClaims(form);
			
			var line = "";
			var lineCount = request.getLineItemCount('custpage_fake_claims');
			if(parseInt(lineCount)>0) {
				for(var i = 1; i <= parseInt(lineCount); i++) {
					var custpage_fakeclaim_date = request.getLineItemValue('custpage_fake_claims', 'custpage_fakeclaim_date', i);
					var custpage_fakeclaim_number = request.getLineItemValue('custpage_fake_claims', 'custpage_fakeclaim_number', i); // Insurance NUMBER / ENTITY
					var custpage_fakeclaim_insintid = request.getLineItemValue('custpage_fake_claims', 'custpage_fakeclaim_insuranceintid', i); // Insurance InternalID
					var custpage_fakeclaim_custname = request.getLineItemValue('custpage_fake_claims', 'custpage_fakeclaim_custname', i);
					var custpage_fakeclaim_custid = request.getLineItemValue('custpage_fake_claims', 'custpage_fakeclaim_custid', i);
					var custpage_fakeclaim_amtdue = request.getLineItemValue('custpage_fake_claims', 'custpage_fakeclaim_amtdue', i);
					var custpage_fakeclaim_payment = request.getLineItemValue('custpage_fake_claims', 'custpage_fakeclaim_payment', i);
					var custpage_fakeclaim_custinvoice = request.getLineItemValue('custpage_fake_claims', 'custpage_fakeclaim_custinvoice', i);
					nlapiLogExecution('debug', 'log line count', "\n############# LINE COUNT : " + i);
					
					line = line + "\n############# LINE COUNT : " + i + "\n";
					line += "DATE : " + custpage_fakeclaim_date + "\n";
					line += "NUMBER : " + custpage_fakeclaim_number + "\n";
					line += "CUSTNAME : " + custpage_fakeclaim_custname + "\n";
					line += "AMTDUE : " + custpage_fakeclaim_amtdue + "\n";
					line += "PAYMENT : " + custpage_fakeclaim_payment + "\n";
					line += "\n\n";
					
					sublistCheckClaimed.setLineItemValue('custpage_fakeclaim_checkdate', i, custpage_fakeclaim_date);
					sublistCheckClaimed.setLineItemValue('custpage_fakeclaim_checknumber', i, custpage_fakeclaim_number);
					sublistCheckClaimed.setLineItemValue('custpage_fakeclaim_checkcustname', i, custpage_fakeclaim_custname);
					sublistCheckClaimed.setLineItemValue('custpage_fakeclaim_checkamtdue', i, custpage_fakeclaim_amtdue);
					sublistCheckClaimed.setLineItemValue('custpage_fakeclaim_checkpayment', i, custpage_fakeclaim_payment);
					
					if(custpage_fakeclaim_payment!=null && parseFloat(custpage_fakeclaim_payment)!=0) {
						if(custpage_fakeclaim_custname!=null && custpage_fakeclaim_custname.trim()!="") {
							//CREDIT MEMO for Insurance Company ----------------------------------------------------------------------------------------
							var customerInvoiceRec = nlapiLoadRecord('invoice', custpage_fakeclaim_insintid, null); // claimbycustomer
							if(customerInvoiceRec!=null) {
								var newCreditMemo = nlapiCreateRecord('creditmemo');
									newCreditMemo.setFieldValue('customform', 157);
									newCreditMemo.setFieldValue('entity', customerInvoiceRec.getFieldValue('entity'));
									newCreditMemo.setFieldValue('trandate', companyTimeNow);
//									newCreditMemo.setFieldValue('createdfrom', nlapiLookupField('invoice', parseInt(custpage_fakeclaim_insintid), 'tranid'));
									newCreditMemo.setFieldValue('createdfrom', custpage_fakeclaim_number); //custpage_fakeclaim_insintid custpage_fakeclaim_number
									nlapiLogExecution('debug', "CLAIM #", custpage_fakeclaim_number + " " + custpage_fakeclaim_insintid);
									//TO REMOVE
									newCreditMemo.setFieldValue('salesrep', customerInvoiceRec.getFieldValue('salesrep'));
									newCreditMemo.setFieldValue('department', (nlapiGetDepartment()!=null) ? nlapiGetDepartment() : 331);
										var custInvoiceOtherRef = (customerInvoiceRec.getFieldValue('otherrefnum')!=null) ? customerInvoiceRec.getFieldValue('otherrefnum') : "";
									newCreditMemo.setFieldValue('otherrefnum', custInvoiceOtherRef); //trim the #tranid
									newCreditMemo.setFieldValue('memo', refNumParam);
									newCreditMemo.setFieldValue('account', customerInvoiceRec.getFieldValue('account'));
									newCreditMemo.setFieldValue('location', 1); //Prism optical FL
									newCreditMemo.selectNewLineItem('item');
									newCreditMemo.setCurrentLineItemValue('item', 'item', 23915);
									newCreditMemo.setCurrentLineItemValue('item', 'quantity', 1);
									newCreditMemo.setCurrentLineItemValue('item', 'description', "Insurance Claim");
									newCreditMemo.setCurrentLineItemValue('item', 'amount', parseFloat(custpage_fakeclaim_amtdue));
									newCreditMemo.commitLineItem('item');
								var createdNewCreditMemo = nlapiSubmitRecord(newCreditMemo, false, false);
								nlapiLogExecution('DEBUG', "CREDIT MEMO FOR INSURANCE COMPANY", createdNewCreditMemo);
								var preloadedCreditMemo = nlapiLoadRecord('creditmemo', createdNewCreditMemo, null);
									var tranidSearchCreditMemo = preloadedCreditMemo.findLineItemValue('apply', 'refnum', custpage_fakeclaim_number); //customerInvoiceRec.getFieldValue('tranid')
									var cntLineItem = preloadedCreditMemo.getLineItemCount('apply');
									for(var x=1; x<=cntLineItem; x++) {
										preloadedCreditMemo.selectLineItem('apply', x);
										preloadedCreditMemo.setCurrentLineItemValue('apply', 'amount', null);
										preloadedCreditMemo.setCurrentLineItemValue('apply', 'apply', 'F');
										preloadedCreditMemo.commitLineItem('apply');
										nlapiLogExecution('debug', 'unappliedLineitem', 'unappliedLineitem');
									}
									preloadedCreditMemo.selectLineItem('apply', tranidSearchCreditMemo);
									preloadedCreditMemo.setCurrentLineItemValue('apply', 'amount', parseFloat(custpage_fakeclaim_payment));
									preloadedCreditMemo.setCurrentLineItemValue('apply', 'apply', 'T');
									preloadedCreditMemo.commitLineItem('apply');
								nlapiLogExecution('debug', 'tranidSearchCreditMemo', tranidSearchCreditMemo);
								
								var updatePreloadCreditMemo = nlapiSubmitRecord(preloadedCreditMemo, true, false);
								
	//							var createdNewCreditMemoEntity = nlapiLoadRecord('creditmemo', createdNewCreditMemo);
								
								//VOID CREDIT MEMO ----------------------------------------------------------------------------------------
								var custTrimEntity = custpage_fakeclaim_custname.substring(0, custpage_fakeclaim_custname.indexOf(" "));
								filtersClaims = [
						       	      new nlobjSearchFilter('formulatext', null, 'contains', insuranceCustomerNameParam).setFormula('{memo}'),
						       	      new nlobjSearchFilter('formulatext', null, 'contains', custTrimEntity).setFormula('{entity}')
						       	];
								var srch = nlapiSearchRecord('transaction', 'customsearch808', filtersClaims, null);
								for (var b = 1; srch!=null && b <= srch.length; b++) {
									var searchresultE = srch[ b-1 ];
									var columnsE = searchresultE.getAllColumns();
									var recordOrigCustInternalID = searchresultE.getValue(columnsE[12]);
									var origCustRecord = nlapiLoadRecord('creditmemo', recordOrigCustInternalID);
										origCustRecord.setFieldValue('voided', 'T');
									var updateOrigCustRecordVOIDING_TRUE = nlapiSubmitRecord(origCustRecord, false, false);
									
									var updateOrigCustRecordVOIDING_TRUEEntity = nlapiLoadRecord('creditmemo', updateOrigCustRecordVOIDING_TRUE);
									if(updateOrigCustRecordVOIDING_TRUEEntity.getFieldValue('voided') == "F") nlapiDeleteRecord('creditmemo', updateOrigCustRecordVOIDING_TRUE);
									else nlapiLogExecution('debug', 'DIDNT VOIDED / DELETE IT');
									nlapiLogExecution('debug', 'Deleted CreditMemo', updateOrigCustRecordVOIDING_TRUE);
								}
								if(srch==null) {
									nlapiLogExecution('debug', 'DIDNT VOIDED / DELETE IT');
								}
								
								//PAYMENT ----------------------------------------------------------------------------------------
								var newPayment = nlapiCreateRecord('customerpayment');
									newPayment.setFieldValue('customform', 135);
									newPayment.setFieldValue('trandate', companyTimeNow);
								var custrecid = nlapiLoadRecord('customer', custpage_fakeclaim_custid);
									newPayment.setFieldText('customer', custrecid.getFieldValue('entityid')); //custpage_fakeclaim_custid 
									newPayment.setFieldValue('department', (nlapiGetDepartment()!=null) ? nlapiGetDepartment() : 331);
									newPayment.setFieldValue('location', 1); //Prism optical FL
									newPayment.setFieldValue('paymentmethod', 15); //PAYMENT METHOD "INSURANCE CHECK"
									newPayment.setFieldValue('checknum', refNumParam);
									newPayment.setFieldValue('payment', parseFloat(custpage_fakeclaim_payment));
									newPayment.setFieldValue('autoapply', 'F');
									newPayment.setFieldValue('memo', insuranceCustomerNameParam);
									
								nlapiLogExecution('debug', 'tranid', customerInvoiceRec.getFieldValue('tranid'));	
									
								var createdNewPayment = nlapiSubmitRecord(newPayment, true, false);
								
								var preloadedPayment = nlapiLoadRecord('customerpayment', createdNewPayment, null);
								var customerInvoiceClaimedRec = nlapiLoadRecord('invoice', custpage_fakeclaim_custinvoice, null);
								var tranidSearch = preloadedPayment.findLineItemValue('apply', 'refnum', customerInvoiceClaimedRec.getFieldValue('tranid')); //customerInvoiceRec.getFieldValue('tranid')
									preloadedPayment.selectLineItem('apply', tranidSearch);
									preloadedPayment.setCurrentLineItemValue('apply', 'amount', parseFloat(custpage_fakeclaim_payment));
									preloadedPayment.setCurrentLineItemValue('apply', 'apply', 'T');
									preloadedPayment.commitLineItem('apply');
									
								var updatePreloadPayment = nlapiSubmitRecord(preloadedPayment, true, false);
								nlapiLogExecution('DEBUG', "PAYMENT CREATE/UPDATE PRELOADED", updatePreloadPayment);
								
	//							var createdNewPaymentEntity = nlapiLoadRecord('customerpayment', createdNewPayment);
								
								//UNDERPAYMENT - Creation of new Invoice for Customer ----------------------------------------------------------------------------------------
//								if(parseFloat(custpage_fakeclaim_payment) < parseFloat(custpage_fakeclaim_amtdue)) { //UNDERPAYMENT
									//REMOVE THIS FOR BUSINESS REQUIREMENT NEEDED
//									var underPayVal = parseFloat(custpage_fakeclaim_amtdue) - parseFloat(custpage_fakeclaim_payment);
//									
//									var underPayCustInvoice = nlapiCreateRecord('invoice');
//										underPayCustInvoice.setFieldValue('customform', 148);
//									var custrecid = nlapiLoadRecord('customer', custpage_fakeclaim_custid);
//										underPayCustInvoice.setFieldText('entity', custrecid.getFieldValue('entityid'));
//										underPayCustInvoice.setFieldValue('trandate', companyTimeNow);
//										underPayCustInvoice.setFieldValue('department', (nlapiGetDepartment()!=null) ? nlapiGetDepartment() : 331);
//										//TO REMOVE
//										underPayCustInvoice.setFieldValue('salesrep', customerInvoiceRec.getFieldValue('salesrep'));
//										underPayCustInvoice.setFieldValue('opportunity', customerInvoiceRec.getFieldValue('opportunity'));
//										underPayCustInvoice.setFieldValue('otherrefnum', custInvoiceOtherRef);
//										underPayCustInvoice.setFieldValue('account', customerInvoiceRec.getFieldValue('account'));
//										underPayCustInvoice.setFieldValue('custbody110', companyTimeNow);
//										underPayCustInvoice.selectNewLineItem('item');
//										underPayCustInvoice.setCurrentLineItemValue('item', 'item', 1397);
//										underPayCustInvoice.setCurrentLineItemValue('item', 'quantity', 1);
//										underPayCustInvoice.setCurrentLineItemValue('item', 'description', "Insurance Claim");
//										underPayCustInvoice.setCurrentLineItemValue('item', 'amount', parseFloat(underPayVal));
//										underPayCustInvoice.commitLineItem('item');
//									var createdUnderPayCustInvoice = nlapiSubmitRecord(underPayCustInvoice, false, false);
//	
//									var createdUnderPayCustInvoiceEntity = nlapiLoadRecord('invoice', createdUnderPayCustInvoice);
//								}
								
								var customerInvoiceClaimedRec2 = nlapiLoadRecord('invoice', custpage_fakeclaim_custinvoice, null);
								customerInvoiceClaimedRec2.setFieldText('custbody37', 'Paid'); // Paid
								var updatedOrigCustRecordStatus = nlapiSubmitRecord(customerInvoiceClaimedRec2, false, false);
							}
						} else {
							//CUSTOMER NAME NOT FOUND
							nlapiLogExecution('debug', 'NO CUSTOMER NAME ON REF/MEMO', null);
						}
					}
				}
			}
			
			data = 	"CUST_ID : " + categoryParam + "\n" +
					"insuranceCustomerIDParam : " + insuranceCustomerIDParam + "\n" +
					"insuranceCustomerNameParam : " + insuranceCustomerNameParam + "\n" +
					"refNumParam : " + refNumParam + "\n" +
					"SUBLIST OBJECT : " + "\n" + line;
			
			var text = form.addField('custpage_test', 'longtext', 'DATA', null, null).setDisplayType('hidden');
				//To remove : uncomment line below for testing
				text.setDefaultValue(data);
				
			
			
			response.writePage(form);
		} else {
		// You refreshed / reloaded
			var formTitle = "Insurance Claims Payments has been Reloaded. Nothing to submit.";
			var form = nlapiCreateForm(formTitle, false);
			response.writePage(form);
		}
	}
}

function createSublistCheckClaims(form) {
	var fakelist=form.addSubList('custpage_fake_checkclaims', 'list', 'Pament Claimed', 'vendorSublistGroup1');
		fakelist.setLabel("Payment Claimed");
	
		fakelist.addField('custpage_fakeclaim_checkdate','text','Date').setDisplayType('inline');
		fakelist.addField('custpage_fakeclaim_checknumber','text','Number').setDisplayType('inline');
		fakelist.addField('custpage_fakeclaim_checkcustname','text','Customer Name').setDisplayType('inline');
		fakelist.addField('custpage_fakeclaim_checkamtdue','currency','Amount Due').setDisplayType('inline');
		fakelist.addField('custpage_fakeclaim_checkpayment','currency','Actual Payment').setDisplayType('inline');
		
		return fakelist;
}

function createSublistClaims(form) {
	var fakelist=form.addSubList('custpage_fake_claims', 'list', 'Pending Claims', 'vendorSublistGroup1');
		fakelist.setLabel("Pending Claims");
	
		fakelist.addField('custpage_fakeclaim_date','text','Date').setDisplayType('inline');
		fakelist.addField('custpage_fakeclaim_number','text','Number').setDisplayType('inline');
		fakelist.addField('custpage_fakeclaim_insuranceintid','text','INTERNAL ID OF INSURANCE CLAIM').setDisplayType('hidden');
		fakelist.addField('custpage_fakeclaim_custname','text','Customer Name').setDisplayType('inline');
		fakelist.addField('custpage_fakeclaim_custid','text','Customer INTERNALID').setDisplayType('hidden');
		fakelist.addField('custpage_fakeclaim_amtdue','currency','Amount Due').setDisplayType('inline');
		fakelist.addField('custpage_fakeclaim_payment','currency','Actual Payment').setDisplayType('entry');
		fakelist.addField('custpage_fakeclaim_custinvoice','text','Claimed from Customer Invoice').setDisplayType('hidden');
		
		return fakelist;
}

function setSublistClaims(sublistClaims, cust) {
	var filtersClaims;
	if(cust!=null && cust!="") {
		filtersClaims = [
       	      new nlobjSearchFilter('formulatext', null, 'contains', cust).setFormula('{name}')
       	];
	}
	var srch = nlapiSearchRecord('invoice', 'customsearch_ss_open_insclaims', filtersClaims, null);
	for (var i = 1; srch!=null && i <= srch.length; i++) {
		var searchresultE = srch[ i-1 ];
		var columnsE = searchresultE.getAllColumns();
		
		var custName = searchresultE.getValue(columnsE[2]);
		var custTrimEntity = custName.substring(0, custName.indexOf(" "));
		var custID = "";
		filtersCustomer = [
      	      new nlobjSearchFilter('formulatext', null, 'is', custTrimEntity).setFormula('{entitynumber}')
      	];
		var srchCust = nlapiSearchRecord('customer', 'customsearch809', filtersCustomer, null);
		for (var a = 1; srchCust!=null && a <= srchCust.length; a++) {
			var searchresultCust = srchCust[ a-1 ];
			var columnsCust = searchresultCust.getAllColumns();
			custID = searchresultCust.getValue(columnsCust[0]);
		}
		var custrecid = nlapiLoadRecord('customer', custID);
		if(custID!=null && custID!="") {
			sublistClaims.setLineItemValue('custpage_fakeclaim_custid', i, custID); // CustomerID
			sublistClaims.setLineItemValue('custpage_fakeclaim_date', i, searchresultE.getValue(columnsE[3])); //name employees
			sublistClaims.setLineItemValue('custpage_fakeclaim_custname', i, searchresultE.getValue(columnsE[2])); //name employees
			sublistClaims.setLineItemValue('custpage_fakeclaim_number', i, searchresultE.getValue(columnsE[1])); //name employees
			sublistClaims.setLineItemValue('custpage_fakeclaim_insuranceintid', i, searchresultE.getValue(columnsE[0])); //name employees
			sublistClaims.setLineItemValue('custpage_fakeclaim_amtdue', i, searchresultE.getValue(columnsE[7])); //name employees
			sublistClaims.setLineItemValue('custpage_fakeclaim_custinvoice', i, searchresultE.getValue(columnsE[10])); //name employees
		} else {
			continue;
		}
	}
}

// THIS PORTION IS EXCLUDED IN BUSINESS REQUIREMENTS

function createSublistPayInfo(form) {
	var fakelist=form.addSubList('custpage_fake_payinfo', 'staticlist', 'Request for Quotation Listings : Dummy 1', 'vendorSublistGroup1');
		fakelist.setLabel("Previous Payments");
		fakelist.addField('custpage_fakepay_name','text','Insurance Name').setDisplayType('inline');
		fakelist.addField('custpage_fakepay_number','text','Internal ID').setDisplayType('inline');
		fakelist.addField('custpage_fakepay_date','date','Date').setDisplayType('inline');
		fakelist.addField('custpage_fakepay_paymentmethod','textarea','Payment Method').setDisplayType('inline');
		fakelist.addField('custpage_fakepay_referencenum','textarea','Reference #').setDisplayType('inline');
		return fakelist;
}

function setSublistPayInfo(sublistPayInfo, cust) {
	var filtersPayInfo = [
        new nlobjSearchFilter('formulatext', null, 'contains', cust).setFormula('{name}')
    ];
	var srchPayInfo = nlapiSearchRecord('transaction', 'customsearch_ss_cust_suitelet_payinfo', filtersPayInfo, null);
	for (var i = 1; srchPayInfo!=null && i <= srchPayInfo.length; i++) {
		var searchresultPayInfo = srchPayInfo[ i-1 ];
		var columnsPayInfo = searchresultPayInfo.getAllColumns();
		
		sublistPayInfo.setLineItemValue('custpage_fakepay_name', i, searchresultPayInfo.getText(columnsPayInfo[1]));
		sublistPayInfo.setLineItemValue('custpage_fakepay_paymentmethod', i, searchresultPayInfo.getText(columnsPayInfo[2]));
		sublistPayInfo.setLineItemValue('custpage_fakepay_referencenum', i, searchresultPayInfo.getValue(columnsPayInfo[3]));
		sublistPayInfo.setLineItemValue('custpage_fakepay_number', i, searchresultPayInfo.getValue(columnsPayInfo[4]));
		sublistPayInfo.setLineItemValue('custpage_fakepay_date', i, searchresultPayInfo.getValue(columnsPayInfo[5]));
	}
}