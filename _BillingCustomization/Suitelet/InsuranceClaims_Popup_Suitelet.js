/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 May 2015     Jose Castro
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response){
	var requestMethod = request.getMethod();
	
	if(requestMethod == 'GET') { }
	else if(requestMethod == 'POST') {
		var amtTotalInsured = request.getParameter('inputAmount');
		var primaryInsuranceName = request.getParameter('primaryInsuranceField');
		var primaryInsuranceID = request.getParameter('primaryInsuranceID');
		var secondaryInsuranceName = request.getParameter('secondaryInsuranceField');
		var secondaryInsuranceID = request.getParameter('secondaryInsuranceID');
		var customerInvoice = request.getParameter('customerInvoice');
		var createdFrom = request.getParameter('createdFrom');
		
		var isPrimaryChecked = request.getParameter('primaryTermsChecked');
		var isSecoindaryChecked = request.getParameter('secondaryTermsChecked');
		
		var customerInvoiceRec = nlapiLoadRecord('invoice', customerInvoice, null);
		
		var dateTimeZoneUtil = Object.create(DateTimeZoneUtils);
		var companyTimeNow = dateTimeZoneUtil.getCompanyCurrentDate();
		
		var checkInsuranceCheckbox = (isPrimaryChecked!=null && isPrimaryChecked=='true') || (isSecoindaryChecked!=null && isSecoindaryChecked=='true');
		var newInsuranceInvoiceID = "";
		if(checkInsuranceCheckbox && customerInvoiceRec!=null) {
			if(isPrimaryChecked!=null && isPrimaryChecked=='true') {
				newInsuranceInvoiceID = primaryInsuranceID;
			} else if(isSecoindaryChecked!=null && isSecoindaryChecked=='true') {
				newInsuranceInvoiceID = secondaryInsuranceID;
			}
			
			var createdFromRec = nlapiLoadRecord('salesorder', createdFrom, null);
			var salesRepFromSalesOrder = createdFromRec.getFieldValue('salesrep');
			
			var newRecInsuranceInvoice = nlapiCreateRecord('invoice', null);
			newRecInsuranceInvoice.setFieldValue('customform', 148);
			newRecInsuranceInvoice.setFieldValue('entity', newInsuranceInvoiceID);
			newRecInsuranceInvoice.setFieldValue('custbody_custinvoice_insuranceclaim', customerInvoice);
			newRecInsuranceInvoice.setFieldValue('trandate', companyTimeNow);
				var custInvoiceDep = customerInvoiceRec.getFieldValue('department');
			newRecInsuranceInvoice.setFieldValue('department', custInvoiceDep);
			newRecInsuranceInvoice.setFieldValue('salesrep', (salesRepFromSalesOrder!=null) ? salesRepFromSalesOrder : null);
			newRecInsuranceInvoice.setFieldValue('opportunity', customerInvoiceRec.getFieldValue('opportunity'));
				var custInvoiceEntity = (customerInvoiceRec.getFieldText('entity')!=null) ? customerInvoiceRec.getFieldText('entity') : "";
				var custInvoiceOtherRef = (customerInvoiceRec.getFieldValue('otherrefnum')!=null) ? customerInvoiceRec.getFieldValue('otherrefnum') : "";
			newRecInsuranceInvoice.setFieldValue('otherrefnum', custInvoiceEntity + " " + custInvoiceOtherRef);
			newRecInsuranceInvoice.setFieldValue('account', customerInvoiceRec.getFieldValue('account'));
			newRecInsuranceInvoice.setFieldValue('custbody110', companyTimeNow);
			newRecInsuranceInvoice.selectNewLineItem('item');
			newRecInsuranceInvoice.setCurrentLineItemValue('item', 'item', 23915);
			newRecInsuranceInvoice.setCurrentLineItemValue('item', 'quantity', 1);
			newRecInsuranceInvoice.setCurrentLineItemValue('item', 'description', "Insurance Claim");
			newRecInsuranceInvoice.setCurrentLineItemValue('item', 'amount', amtTotalInsured);
			newRecInsuranceInvoice.commitLineItem('item');

			var createdNewInsuranceInvoice = nlapiSubmitRecord(newRecInsuranceInvoice, false, false);
			
			var newRecCreditMemo = nlapiTransformRecord('invoice', customerInvoice, 'creditmemo'); //nlapiCreateRecord('creditmemo', null);
			newRecCreditMemo.setFieldValue('customform', 157);
			newRecCreditMemo.setFieldValue('entity', customerInvoiceRec.getFieldValue('entity'));
			newRecCreditMemo.setFieldValue('trandate', companyTimeNow);
			newRecCreditMemo.setFieldValue('createdfrom', customerInvoiceRec.getFieldValue('internalid')); //internalid //entity //tranid //customerInvoice //newRecInsuranceInvoice
			newRecCreditMemo.setFieldValue('salesrep', (salesRepFromSalesOrder!=null) ? salesRepFromSalesOrder : null);
			newRecCreditMemo.setFieldValue('department', custInvoiceDep);
			var otherrefnum_all = custInvoiceOtherRef + " " + newRecInsuranceInvoice.getFieldText('entity');
			newRecCreditMemo.setFieldValue('otherrefnum', otherrefnum_all.substring(0,45)); //trim the #tranid
			newRecCreditMemo.setFieldValue('memo', "Pending Filed - " + companyTimeNow + " - " + newRecInsuranceInvoice.getFieldText('entity'));
			newRecCreditMemo.setFieldValue('account', newRecInsuranceInvoice.getFieldValue('account'));
			newRecCreditMemo.setFieldValue('location', 1); //Prism optical FL
			
			newRecCreditMemo.setFieldValue('taxtotal', 0);
			newRecCreditMemo.setFieldValue('altshippingcost', 0);
			newRecCreditMemo.setFieldValue('shippingcost', 0);
			newRecCreditMemo.setFieldValue('handlingcost', 0);
			newRecCreditMemo.setFieldValue('althandlingcost', 0);
			newRecCreditMemo.setFieldValue('transactiondiscount', 'F');
			
			newRecCreditMemo.selectNewLineItem('item');
			newRecCreditMemo.setCurrentLineItemValue('item', 'item', 23915);
			newRecCreditMemo.setCurrentLineItemValue('item', 'quantity', 1);
			newRecCreditMemo.setCurrentLineItemValue('item', 'description', "Insurance Claim");
			newRecCreditMemo.setCurrentLineItemValue('item', 'amount', amtTotalInsured);
			newRecCreditMemo.commitLineItem('item');

			var createdNewCreditMemo = nlapiSubmitRecord(newRecCreditMemo, false, true);
			
			var preloadedCreditMemo = nlapiLoadRecord('creditmemo', createdNewCreditMemo, null);
				var countPreload = preloadedCreditMemo.getLineItemCount('item');
				var tranidSearch = preloadedCreditMemo.findLineItemValue('apply', 'refnum', customerInvoiceRec.getFieldValue('tranid'));
				var insuranceClaimDescriptionSearch = preloadedCreditMemo.findLineItemValue('item', 'description', 'Insurance Claim');
			for(var z=countPreload;countPreload!=null && z>=1;z--) {
				if(z!=insuranceClaimDescriptionSearch) {
					preloadedCreditMemo.removeLineItem('item', z);
				}
			}
			preloadedCreditMemo.selectLineItem('apply', tranidSearch);
			preloadedCreditMemo.setCurrentLineItemValue('apply', 'amount', amtTotalInsured);
			preloadedCreditMemo.commitLineItem('apply');
			var updatePreloadedCreditMemo = nlapiSubmitRecord(preloadedCreditMemo, false, true);
			
			if(createdNewInsuranceInvoice!=null && createdNewCreditMemo!=null) {
				var custRecordNewCreditMemo = nlapiLoadRecord('invoice', customerInvoice, null);
				custRecordNewCreditMemo.setFieldText('custbody37', 'Pending'); // Pending
				var updatedCustRecordStatus = nlapiSubmitRecord(custRecordNewCreditMemo, false, false);
			}
			
			if(createdNewInsuranceInvoice!=null && createdNewCreditMemo!=null) {
				var form = nlapiCreateForm("Claim has been Submitted", true);
					form.addFieldGroup('custpage_fgroup', 'The following transactions have been automatically Created as part of this Insurance Claim:', null).setSingleColumn(true);
					var insuranceText = form.addField('custpage_insuranceinvoice', 'textarea', 'Insurance Company Invoice #:', null, 'custpage_fgroup');
						insuranceText.setDisplayType('inline');
						var checkInsuranceCreated = nlapiLoadRecord('invoice', createdNewInsuranceInvoice, null);
						var insuranceInvoiceNumComplete = checkInsuranceCreated.getFieldValue('tranid') + " " + checkInsuranceCreated.getFieldText('entity');
						insuranceText.setDefaultValue(insuranceInvoiceNumComplete);
					var creditMemoText = form.addField('custpage_creditmemo', 'textarea', 'Customer Credit Memo #:', null, 'custpage_fgroup');
						creditMemoText.setDisplayType('inline');
						var checkCreditMemoCreated = nlapiLoadRecord('creditmemo', createdNewCreditMemo, null);
						var creditMemoNumComplete = checkCreditMemoCreated.getFieldValue('tranid') + " " + checkCreditMemoCreated.getFieldText('entity');
						creditMemoText.setDefaultValue(creditMemoNumComplete);
				response.writePage(form);
			} else {
				var form = nlapiCreateForm("Insurance Submission : Unsuccessful", true);
					form.addField('custpage_error', 'label', "UNSUCCESSFUL CREATION OF INSURANCE INVOICE & CREDIT MEMO", null, null);
				response.writePage(form);
			}
		} else {
			var form = nlapiCreateForm("Insurance Submission : Unsuccessful", true);
				form.addField('custpage_error', 'label', "Un-selected Insurance Company for Claim | Customer Invoice cannot be found.", null, null);
			response.writePage(form);
		}
	}
}